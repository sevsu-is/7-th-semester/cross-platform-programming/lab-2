/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 6.5.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>
#include "label.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QPlainTextEdit *sourceTextEdit;
    QPlainTextEdit *targetTextEdit;
    QLabel *starsCount;
    Label *label;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QLineEdit *windowTitleField;
    QPushButton *submitButton;
    QMenuBar *menubar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName("MainWindow");
        MainWindow->resize(528, 318);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName("centralwidget");
        sourceTextEdit = new QPlainTextEdit(centralwidget);
        sourceTextEdit->setObjectName("sourceTextEdit");
        sourceTextEdit->setGeometry(QRect(40, 140, 151, 111));
        targetTextEdit = new QPlainTextEdit(centralwidget);
        targetTextEdit->setObjectName("targetTextEdit");
        targetTextEdit->setEnabled(false);
        targetTextEdit->setGeometry(QRect(200, 140, 151, 111));
        starsCount = new QLabel(centralwidget);
        starsCount->setObjectName("starsCount");
        starsCount->setGeometry(QRect(380, 140, 141, 18));
        label = new Label(centralwidget);
        label->setObjectName("label");
        label->setGeometry(QRect(40, 260, 191, 18));
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName("label_3");
        label_3->setGeometry(QRect(40, 110, 111, 18));
        label_4 = new QLabel(centralwidget);
        label_4->setObjectName("label_4");
        label_4->setGeometry(QRect(200, 110, 111, 18));
        label_5 = new QLabel(centralwidget);
        label_5->setObjectName("label_5");
        label_5->setGeometry(QRect(31, 11, 90, 18));
        widget = new QWidget(centralwidget);
        widget->setObjectName("widget");
        widget->setGeometry(QRect(31, 30, 411, 38));
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setObjectName("horizontalLayout");
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        windowTitleField = new QLineEdit(widget);
        windowTitleField->setObjectName("windowTitleField");

        horizontalLayout->addWidget(windowTitleField);

        submitButton = new QPushButton(widget);
        submitButton->setObjectName("submitButton");

        horizontalLayout->addWidget(submitButton);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName("menubar");
        menubar->setGeometry(QRect(0, 0, 528, 28));
        MainWindow->setMenuBar(menubar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        starsCount->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        label->setText(QString());
        label_3->setText(QCoreApplication::translate("MainWindow", "Source text:", nullptr));
        label_4->setText(QCoreApplication::translate("MainWindow", "Target Text:", nullptr));
        label_5->setText(QCoreApplication::translate("MainWindow", "Window title:", nullptr));
        submitButton->setText(QCoreApplication::translate("MainWindow", "Submit", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
