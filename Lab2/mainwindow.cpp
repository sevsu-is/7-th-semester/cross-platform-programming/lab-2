#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include <QString>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->sourceTextEdit, SIGNAL(textChanged()), this, SLOT(copyTextFromSourceToTargetTextEdit()));
    connect(ui->targetTextEdit, SIGNAL(textChanged()), this, SLOT(countStarSymbolsInTargetTextEdit()));
    connect(ui->label, SIGNAL(numIsGreaterThan10()), this, SLOT(disableSourceTextEdit()));
    ui->label->setParent(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_submitButton_released()
{
    setWindowTitle(ui->windowTitleField->text());
}

void MainWindow::copyTextFromSourceToTargetTextEdit() {
    QString sourceText = ui->sourceTextEdit->toPlainText();
    QString modifiedText = sourceText.replace('a', '*');
    modifiedText = sourceText.replace('A', '*');
    ui->targetTextEdit->setPlainText(modifiedText);
}

void MainWindow::countStarSymbolsInTargetTextEdit() {
    QString targetText = ui->targetTextEdit->toPlainText();
    int numberOfStars = targetText.count('*');
    ui->starsCount->setText(QString("Number of '*': ") + QString::number(numberOfStars));
}

void MainWindow::on_sourceTextEdit_textChanged()
{
    QString targetText = ui->targetTextEdit->toPlainText();
    int numberOfStars = targetText.count('*');

    if (numberOfStars > 8) {
        emit ui->label->numIsGreaterThan10();
    }
}

void MainWindow::disableSourceTextEdit() {
    ui->sourceTextEdit->setReadOnly(true);
    ui->label->setText("Field is now read-only!");
}

