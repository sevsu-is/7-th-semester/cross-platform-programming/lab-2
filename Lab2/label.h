#pragma once
#include <QLabel>

class Label : public QLabel
{
    Q_OBJECT
signals:
    void numIsGreaterThan10();
public:
    Label(QWidget *parent);
};
